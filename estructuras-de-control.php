<!DOCTYPE html>
<html>
<head>
	<title>Estructuras de control en PHP</title>
</head>
<body>

<?php

	/* Las estructuras de control en leguajes de programación 
		se utilizan para realizar operaciones por ejemplo: selectivas, repetitivas, y ambas;
		Las estructuras de control más utilizadas son:

		*	IF (selectiva)
		* 	FOR (repetitivas)
		*	WHILE (repetitivas-codicional)

	*/

		// Estructura de control IF, ejemplo:
		$x = 1;
		$y = 10;
		$z = 5;

		if ($x == 0) {
			echo "La variable es CERO";
		}
		else{
			echo "La variable no es CERO";
		}

		// Estructura FOR, ejemplo:
		$colores = array('blue', 'green', 'yellow', 'violet', 'white', 'black','blue', 'green', 'orange', 'silver', 'white', 'black','blue', 'green', 'yellow', 'violet', 'white', 'black','blue', 'green', 'orange', 'silver', 'white', 'black','blue', 'green', 'yellow', 'violet', 'white', 'black','blue', 'green', 'orange', 'silver', 'white', 'black','blue', 'green', 'yellow', 'violet', 'white', 'black','blue', 'green', 'orange', 'silver', 'white', 'black');

		// función count(), y sirve para contar los lementos de un vector
		for ($i=0; $i < 4; $i++) { 
			echo "<br />Los numeros naturales hasta el 10: " . $i;
		}

		for ($i=0; $i < 6 ; $i++) { 
			echo "<br />Elmento $i = [" . $colores[$i] . "]";
		}

		for ($i=0; $i < count($colores); $i++) { 
			echo "<br /><h2>Color <h1 style='color:$colores[$i]'>$i : $colores[$i]</h1></h2>";
		}

		// Ejercicio: Crear un vector de 20 elementos con números del 1 al 20, y mostrar sólo los multiplos de 2
		$numeros = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);
		for ($i=0;  $i<= count($numeros); $i=$i+2) { 
			echo "<br /> Multiplos de 2: " . $numeros[$i];
		}


?>

</body>
</html>