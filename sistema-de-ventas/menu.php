<?php
	session_start();
	if (isset($_SESSION['usuario']) && isset($_SESSION['contrasenia'])) {
?>
<!DOCTYPE html>
<html>
<head>
	<title>Menu</title>
</head>
<body>
<div style="margin:0 auto; width:500px;">
	<h3>Bienvenid@<br /> Sistema de Ventas</h3>
	<ul>
		<li><a href="./menu.php">Inicio</a></li>
		<li><a href="./cliente.php">Clientes</a></li>
		<li><a href="./productos.php">Productos</a></li>
		<li><a href="./ventas.php">Ventas</a></li>
		<li><a href="./logout.php">Salir</a></li>
	</ul>
</div>
</body>
</html>
<?php
	}
	else{
	header('Location: index.php');
	}
?>

