<!DOCTYPE html>
<html>
<head>
	<title>Practicas</title>
</head>
<body>
<?php
	/*
		3. realice un programa que recorra un array de nombres y muestre sólo los nombres Carlos y cuántos son. Ej.:

Array("Ana","Carlos","Pedro","José","Carlos","Luis","Jessica","Gabriel","Jhon","Patricia","Rosario","Indira",

"Carlos","Ana","Daniel","Ariel","Alberto","Alonzo","Lucia","Gabriela")

Se debe mostrar en pantalla:

 Carlos se repite 3 veces
	*/	

$nombres = Array("Ana","Carlos","Pedro","José","Carlos","Luis","Jessica","Gabriel","Jhon","Patricia","Rosario","Indira",

"Carlos","Ana","Daniel","Ariel","Alberto","Alonzo","Lucia","Gabriela");

$nom = "Carlos";
$contador = 0;
for ($i=0; $i < count($nombres); $i++) { 
	if ($nombres[$i] == $nom) {
		$contador = $contador + 1;
	}
}

echo "<h2>$nom se repite $contador veces</h2>";

?>
</body>
</html>